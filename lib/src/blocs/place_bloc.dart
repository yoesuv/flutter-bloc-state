import 'package:bloc_state/src/models/PlaceResponse.dart';
import 'package:bloc_state/src/repositories/place_repositories.dart';
import 'package:bloc_state/src/states/ListPlaceState.dart';
import 'package:rxdart/rxdart.dart';

class PlaceBloc {

  final PlaceRepository _repo = PlaceRepository();

  final BehaviorSubject<ListPlaceState> _listPlace = BehaviorSubject<ListPlaceState>();
  Function(ListPlaceState) get changeListPlace => _listPlace.sink.add;
  Stream<ListPlaceState> get streamListPlace => _listPlace.stream;

  Future<void> getListPlace() async {
    changeListPlace(ListPlaceLoading());
    try {
      final ListPlace listPlace = await _repo.getListPlace();
      changeListPlace(ListPlaceLoaded(listPlace));
    } catch (err) {
      changeListPlace(ListPlaceError(err));
    }
  }

}