import 'package:bloc_state/src/data/constants.dart';
import 'package:bloc_state/src/models/PlaceResponse.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ItemPlace extends StatelessWidget {

  const ItemPlace(this._place);

  final Place _place;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _image(),
        _title()
      ],
    );
  }

  Widget _image() {
    return ShaderMask(
      shaderCallback: (Rect rect) {
        return LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: <Color>[Colors.transparent, Colors.black38]
        ).createShader(Rect.fromLTRB(0, 0, rect.width, rect.height));
      },
      blendMode: BlendMode.srcOver,
      child: CachedNetworkImage(
        width: double.infinity,
        height: ITEM_HEIGHT,
        imageUrl: _place.image,
        fit: BoxFit.cover,
        placeholder: (BuildContext context, String str) => Image.asset('assets/images/placeholder_image.png',
          width: double.infinity,
          height: ITEM_HEIGHT,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _title() {
    return Positioned(
      bottom: 0,
      child: Container(
        margin: const EdgeInsets.only(left: 8, bottom: 8),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(_place.name, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
            Text(_place.location, style: TextStyle(fontSize: 18, color: Colors.white)),
          ],
        ),
      ),
    );
  }

}