import 'package:bloc_state/src/blocs/place_bloc.dart';
import 'package:bloc_state/src/states/ListPlaceState.dart';
import 'package:bloc_state/src/widgets/item_place.dart';
import 'package:flutter/material.dart';

class ListPlace extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final PlaceBloc _bloc = PlaceBloc();
    _bloc.getListPlace();

    return Scaffold(
      appBar: AppBar(
        title: Text('List Place', style: TextStyle(fontWeight: FontWeight.bold)),
      ),
      body: _buildList(_bloc),
    );
  }

  Widget _buildList(PlaceBloc bloc) {
    return StreamBuilder<ListPlaceState>(
      stream: bloc.streamListPlace,
      initialData: ListPlaceUninitialized(),
      builder: (BuildContext context, AsyncSnapshot<ListPlaceState> snapshot) {
        final ListPlaceState listPlaceState = snapshot.data;
        if (listPlaceState is ListPlaceUninitialized) {
          return const Center(
            child: Text('Uninitilized'),
          );
        }
        if (listPlaceState is ListPlaceLoading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (listPlaceState is ListPlaceError) {
          return Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text('Error', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                Text(listPlaceState.error.toString()),
              ],
            )
          );
        }
        if (listPlaceState is ListPlaceLoaded) {
         return ListView.builder(
           itemCount: listPlaceState.listPlace.data.length,
           itemBuilder: (BuildContext context, int index) {
             return ItemPlace(listPlaceState.listPlace.data[index]);
           },
         );
        }
        return Container();
      },
    );
  }

}