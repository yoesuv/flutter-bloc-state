import 'package:bloc_state/src/models/PlaceResponse.dart';
import 'package:bloc_state/src/networks/network_helper.dart';
import 'package:dio/dio.dart';

class PlaceRepository {

  final NetworkHelper _networkHelper = NetworkHelper();

  Future<ListPlace> getListPlace() async {
    final Response<dynamic> response = await _networkHelper.get('list_place.json') as Response<dynamic>;
    return ListPlace.fromJson(response.data as Map<String,dynamic>);
  }

}