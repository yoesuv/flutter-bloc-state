class ListPlace {

  ListPlace({this.statusCode, this.data});

  ListPlace.fromJson(Map<String, dynamic> json) {
    statusCode = int.parse(json['status_code'].toString());
    if (json['data'] != null) {
      data = <Place>[];
      json['data'].forEach((dynamic v) {
        data.add(Place.fromJson(v));
      });
    }
  }

  int statusCode;
  List<Place> data;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.map((Place v) => v.toJson()).toList();
    }
    return data;
  }

}

class Place {

  Place({this.name, this.location, this.description, this.image});

  Place.fromJson(dynamic json) {
    name = json['name'].toString();
    location = json['location'].toString();
    description = json['description'].toString();
    image = json['image'].toString();
  }

  String name;
  String location;
  String description;
  String image;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['location'] = location;
    data['description'] = description;
    data['image'] = image;
    return data;
  }
}
