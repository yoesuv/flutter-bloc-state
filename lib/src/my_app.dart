import 'package:flutter/material.dart';
import 'package:bloc_state/src/screens/list_place.dart';

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.teal
      ),
      home: ListPlace(),
    );
  }

}