import 'package:bloc_state/src/models/PlaceResponse.dart';

abstract class ListPlaceState{
  const ListPlaceState();
}

// init
class ListPlaceUninitialized extends ListPlaceState{}
// loading
class ListPlaceLoading extends ListPlaceState{}
// error
class ListPlaceError extends ListPlaceState{
  const ListPlaceError(this.error);
  final Object error;
  @override
  String toString() {
    return error.toString();
  }
}
// loaded
class ListPlaceLoaded extends ListPlaceState{
  const ListPlaceLoaded(this.listPlace);
  final ListPlace listPlace;
  @override
  String toString() {
    return listPlace.toJson().toString();
  }
}

