## Flutter App with state model 1.0.0

Flutter project with Presentation layer to show data / build widget

check ```lib/src/states/ListPlaceState```

#### References
- [codingwithjoe](https://codingwithjoe.com/flutter-building-a-widget-with-streambuilder/)
- [bloclibrary](https://bloclibrary.dev/#/flutterinfinitelisttutorial)

#### Flutter dependencies
```
Flutter 1.12.13+hotfix.9 • channel stable • https://github.com/flutter/flutter.git
Framework • revision f139b11009 (4 weeks ago) • 2020-03-30 13:57:30 -0700
Engine • revision af51afceb8
Tools • Dart 2.7.2
```
#### Screenshot
![screenshot](https://i.imgur.com/Ly9QX5b.png)

#### List Library
- [cached_network_image](https://pub.dev/packages/cached_network_image)
- [dio](https://pub.dev/packages/dio)
- [equatable](https://pub.dev/packages/equatable)
- [rx dart](https://pub.dev/packages/rxdart)
